var events = require('events'),
    fs = require('fs'),
    https = require('https'),
    crypto = require('crypto'),
    uuid = require('node-uuid'),
    xml2json = require('xml2json'),
    lo = require('lodash'),
    util = require('util'),
    inspect = require('eyes').inspector({styles: {all: 'magenta'}, maxLength: 16384});

var Clink = function () {
    'use strict';
    events.EventEmitter.call(this);
    //general flow of things

    var self = this;
	 
    this.init = function (options) {
        self.log = options.log;
        self.config = options.config;
    };

    self.parserOptions = {
        object: true,
        reversible: true,
        coerce: false,
        sanitize: false,
        trim: false
    };

    self.prepParams = function () {
        var now = new Date().getTime(),
            guid = uuid.v1(),
            preHash = self.config.get('api_key') + self.config.get('ip_address') + now + guid + self.config.get('shared_key'),
            md5hash = crypto.createHash('md5').update(preHash).digest('hex'),
            newParams = {};
        newParams.apikey = self.config.get('api_key');
        newParams.time = now;
        newParams.random = guid;
        newParams.hash = md5hash;
        //self.log.info('prepParams', newParams);
        return newParams;
    };

    self.parseOrgs = function (doc) {
        self.log.info("Beginning xml parse");
        var xmlDoc = xml2json.toJson(doc, self.parserOptions),
            orgs = {};
        try {
            orgs = xmlDoc.clwebService.results.page.items.organization;
        } catch (parseError) {
            self.log.error("Error while parsing response from CollegiateLink");
            self.log.error(parseError);
            inspect(parseError);
            inspect(doc);
        }
        lo.each(orgs, function (org) {
            var orgInfo = {},
                customFields = org.customfields.customfield,
                customFieldVals = lo.map(customFields, function (item) {
                    var result = "";
                    if (item.values.string) {
                        result = item.values.string.$t;
                    }
                    return result;
                }),
                shortName = org.shortName.$t;
            if (shortName) { shortName = shortName.trim(); }
            orgInfo.name = org.name.$t.trim();
            orgInfo.full_name = orgInfo.name + ' (' + shortName + ')';
            orgInfo.siteURL = org.siteUrl.$t;
            orgInfo.short = org.shortName.$t;
            orgInfo.cl_id = org.id.$t;
            orgInfo.status = org.status.$t;
            orgInfo.account_id = customFieldVals[0];
            orgInfo.ein = customFieldVals[1];
            orgInfo.bba = customFieldVals[2];
            orgInfo.wa2012 = customFieldVals[3];
            orgInfo.gosa = customFieldVals[4];
            orgInfo.smrmt = customFieldVals[5];
            orgInfo.smrmt_plan = customFieldVals[6];
            //inspect(org);
            //inspect(orgInfo);
            self.emit('orgInfo', orgInfo);
        });
        self.emit('parseDone');
    };
		
    this.request = function (action) {
        var actions = self.config.get('actions'),
            host = self.config.get('cl_url'),
            host_path = self.config.get('base_uri') + actions.org_list,
            urlParams = self.prepParams(),
            paramStr = "apikey=" + urlParams.apikey + "&random=" + urlParams.random + "&time=" + urlParams.time + "&hash=" + urlParams.hash,
            options = {},
            resp = "",
            full_url = "";

        if (self.config.DEBUG) {
            self.log.debug("data/org_list_five.xml");
            fs.readFile('data/org_list_five.xml', 'UTF-8', function (err, data) {
                if (err) {
                    self.log.error("ERROR while sending stubRequest (readFile): ", err);
                } else {
                    self.emit('orgData', data);
                }
            });
        } else {
            host_path = host_path + paramStr;
            options.host = host;
            options.path = host_path;
            options.method = "GET";
            self.log.info("request get with opts", options);
            full_url = "https://" + host + host_path;
            self.log.info("URL: ", full_url);

            https.get(full_url, function (res) {
            //self.log.info("statusCode: ", res.statusCode);
            //self.log.info("headers: ", res.headers);

                res.on('data', function (d) {
                    resp = resp + d;
                    //process.stdout.write(d);
                });
                res.on('end', function () {
                    self.emit('orgData', resp);
                });
            }).on('error', function (e) {
                self.log.error("ERROR while fetching organizations");
                self.log.error(e);
            });
        }  //end if/else
    };

    /* the flow! */
    this.on('orgData', self.parseOrgs);
};

util.inherits(Clink, events.EventEmitter);
module.exports = new Clink();
