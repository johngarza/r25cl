var events = require('events'),
	fs = require('fs'),
	http = require('http'),
    xml2json = require("xml2json"),
	util = require('util'),
    inspect = require('eyes').inspector({styles: {all: 'magenta'}, maxLength: 16384});

var R25 = function (options) {
    'use strict';
    events.EventEmitter.call(this);
	//general flow of things
	var self = this;

    this.init = function (options) {
        self.log = options.log;
        self.config = options.config;
    };

    /* two public methods
        - getOrgs - takes no arguments, returns a json object of all RSOs from R25WS
        - getOrg - takes an org_id and returns a json object straight from R25WS
        - setOrg - takes an R25WS org json object and sends it back to R25WS
        -putOrg - takes an R25WS xml string and PUT's it against R25WS
	*/
    self.parserOptions = {
        object: true,
        reversible: true,
        coerce: false,
        sanitize: false,
        trim: false
    };

    //utility log our object to a file (it should be an xml string)
	this.logObj = function (org_id, org_obj, suffix) {
		//assumes the passed object is a JSON object
		var fname = "put/org-" + org_id + suffix + ".json", content = JSON.stringify(org_obj, null, 1);
		//inspect(org_obj);
		fs.writeFile(fname, content, function (err) {
			if (err) {
				self.log.error("ERROR while writing put json: ", org_id);
			}
		});
	};

    
	//Straight forward REST GET request on organizations.xml
	this.getOrgs = function () {
        var headers = {}, options = {}, result = {}, resp = "", r25ws_config = self.config.get('r25ws'),
            request_url = "http://" + r25ws_config.r25ws_url + r25ws_config.base_uri + r25ws_config.orgs_GET,
            r25ws_host = r25ws_config.r25ws_url,
            r25ws_path = r25ws_config.base_uri + r25ws_config.orgs_GET,
            r25ws_port = r25ws_config.port || 80,
            req = "";
        
        options.hostname = r25ws_host;
        options.port = r25ws_port;
        options.path = r25ws_path;
        options.method = 'GET';
        options.auth = r25ws_config.username + ":" + r25ws_config.password;

        req = http.request(options, function (res) {
            res.on('data', function (chunk) { resp = resp + chunk; });
            res.on('error', function (err) { self.log.error("ERROR while GETing organizations"); });
            res.on('end', function () {
                var doc = xml2json.toJson(resp, self.parserOptions);
                if (doc["r25:organizations"]) {
                    result.json = doc["r25:organizations"]["r25:organization"];
                    self.emit("getOrgs", result);
                } else {
                    self.emit("getError", {
                        id: "all",
                        error: "important",
                        code: res.statusCode,
                        message: "Unexpected response from GET organizations!",
                        details: "Invalid response to " + request_url + " please investigate.",
                        data: resp
                    });
                }
            });
        });
		req.end();
	};

	/*
	*/
	this.getOrg = function (org_id, callback) {
        var headers = {},
            options = {},
            result = {},
            resp = "",
            req = "",
            r25ws_config = self.config.get('r25ws'),
            request_url = "http://" + r25ws_config.r25ws_url + r25ws_config.base_uri + r25ws_config.org_GET + org_id,
            r25ws_host = r25ws_config.r25ws_url,
            r25ws_port = r25ws_config.port || 80,
            r25ws_path = r25ws_config.base_uri + r25ws_config.org_GET + org_id;

        //headers['Content-Type'] = "text/plain";
        //options.header = headers;
        options.hostname = r25ws_host;
        options.port = r25ws_port;
        options.path = r25ws_path;
        options.method = 'GET';
        options.auth = r25ws_config.username + ":" + r25ws_config.password;

        req = http.request(options, function (res) {
			res.on('data', function (chunk) {
				resp = resp + chunk;
			});
            res.on('error', function (err) {
                self.log.error("ERROR while GETing organization info: ", err);
            });
			res.on('end', function () {
                var doc = xml2json.toJson(resp, self.parserOptions);
                //inspect(doc);
                if (doc["r25:organizations"]) {
                    result.json = doc;
                    self.emit("getOrg", result);
                    if (callback) { callback(result); }
                } else {
                    self.emit("getError", {
                        id: org_id,
                        error: "important",
                        code: res.statusCode,
                        message: "Unexpected response from GET : " + org_id,
                        details: "Invalid response to " + request_url + " please investigate.",
                        data: resp
                    });
                }
            });
        });
        req.end();
	};

	this.putOrg = function (org_id, org_obj, callback, error) {
		var options = {},
            resp = "",
            req = "",
            doc = "",
            errObj = {},
            headers = {},
            r25ws_config = self.config.get('r25ws'),
            put_data = xml2json.toXml(org_obj),
            post_url = "http://" + r25ws_config.r25ws_url + r25ws_config.base_uri + r25ws_config.org_PUT + org_id,
            r25ws_host = r25ws_config.r25ws_url,
            r25ws_port = r25ws_config.port || 80,
            r25ws_path = r25ws_config.base_uri + r25ws_config.org_PUT + org_id;
        //log for debugging
        if (self.config.DEBUG) { self.logObj(org_id, put_data, "-posted"); }

        /* disable PUT request options for now */
		headers['Content-Type'] = "text/plain";
		options.headers = headers;
		options.hostname = r25ws_host;
		options.port = r25ws_port;
		options.path = r25ws_path;
		options.method = 'PUT';
        options.auth = r25ws_config.username + ":" + r25ws_config.password;

        req = http.request(options, function (res) {
			res.on('data', function (chunk) {
				resp = resp + chunk;
			});
			res.on('end', function () {
				try {
                    doc = xml2json.toJson(resp, self.parserOptions);
                    if (doc["r25:results"]) {
                        if (doc["r25:results"]["r25:info"]) {
                            callback({ msg: doc["r25:results"]["r25:info"]["r25:msg"].$t });
                        }
                        if (doc["r25:results"]["r25:error"]) {
                            error({
                                id: org_id,
                                error: "important",
                                code: doc["r25:results"]["r25:error"]["r25:msg_id"].$t,
                                message: doc["r25:results"]["r25:error"]["r25:proc_error"].$t + " : " + org_obj["r25:organizations"]["r25:organization"]["r25:organization_name"].$t,
                                details: doc["r25:results"]["r25:error"]["r25:msg"].$t,
                                data: resp
                            });
                        }
                    } else {
                        error({
                            id: org_id,
                            error: "important",
                            code: "unknown",
                            message: "unexpected result from PUT operation" + " : " + org_obj["r25:organizations"]["r25:organization"]["r25:organization_name"].$t,
                            details: "PUT operation returned an unknown result, please investigate",
                            data: resp
                        });
                    }
				} catch (e) {
					//we may have an xml response, not JSON
					self.log.error("ERROR!");
					inspect(e);
					errObj = {
						id: org_id,
						error: "important",
						code: res.statusCode,
						message: "Unable to update custom attributes for organization: " + org_obj["r25:organizations"]["r25:organization"]["r25:organization_name"].$t,
						details: "Error while saving custom attribute information, XML instead of JSON returned. <code>" + JSON.stringify(doc) + "</code>",
						data: resp
					};
					error(errObj);
				}
			});
		});
		req.on('error', function (err) {
			self.log.error("req.on error:");
			inspect(err);
			errObj = {
				id: org_id,
				error: "important",
				code: "500",
				message: "Error while PUTing organization info: " + org_obj["r25:organizations"]["r25:organization"]["r25:organization_name"].$t,
				details: "Error while making PUT request.",
				data: err
			};
			error(errObj);
		});

		//log the PUT data to a put/file
		//self.logObj(org_id, org_obj, "");
		req.write(put_data);//PUT payload!
		req.end();
	};
    /*
    ** STUB Requests (fixtures) for TESTING ONLY
    */
    this.getOrgStub = function (org_id) {
        var stubFile = "data/organization-" + org_id + ".xml",
            result = {};
        self.log.debug(stubFile);
		fs.readFile(stubFile, 'UTF-8', function (err, data) {
            if (err) {
                self.log.error("ERROR while sending stubRequest (readFile): ", stubFile, err);
            } else {
                try {
                    var doc = xml2json.toJson(data, self.parserOptions);
                    result.json = doc;
                } catch (e) {
                    self.log.error("ERROR while parsing GET data: ", e);
                    self.log.error(data);
                }
                self.emit("getOrg", result);
            }
        });
    };

    this.getOrgsStub = function () {
        var result = {},
            stubFile = "data/organizations.xml";
        fs.readFile(stubFile, 'UTF-8', function (err, data) {
            if (err) {
                self.log.error("ERROR while sending stubRequest (readFile): ", stubFile, err);
            } else {
                try {
                    var doc = xml2json.toJson(data, self.parserOptions);
                    result.json = doc["r25:organizations"]["r25:organization"];
                } catch (e) {
                    self.log.error("ERROR while parsing data: " + e);
                    inspect(e);
                }
                self.emit("getOrgs", result);
            }
        });
    };
};

util.inherits(R25, events.EventEmitter);
module.exports = new R25();