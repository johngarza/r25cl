var flatiron = require('flatiron'),
	app = flatiron.app,
    fs = require('fs'),
	events = require('events'),
    lo = require('lodash'),
    report = require('../report'),
    r25 = require('../r25'),
    clink = require('../clink'),
    util = require('util'),
	nmailer = require('nodemailer'),
    async = require('async');
var inspect = require('eyes').inspector({styles: {all: 'magenta'}, maxLength: 16384});

var kCA0 = "62";  //website custom attribute
var kCA1 = "141"; //Collegiate Link First Seen
var kCA2 = "142"; //Collegiate Link Last Seen
var kCA3 = "143"; //Collegiate Link Last Scan

var Sync  = function sync(org_id) {
    'use strict';
    events.EventEmitter.call(this);
    //data objects
    var self = this,
        r25Orgs = {},
        cLOrgs = {},
        superSet = {},
        r25OrgList = {},
        r25UpdatedOrgs = {},
        prepCLWait = 0;
    
    r25.init({log : app.log, config : app.config});
    clink.init({log : app.log, config : app.config});
    
    self.syncErrors = {};
    self.prepR25Wait = 0;
    self.reportContent = "";
    
    /*******
     * Custom Attribute modification methods
     *******/
    //fetch a specific attribute id set from a prep'ed custom attribute array
    self.getCA = function (id, caArray) {
        var result;
        lo.each(caArray, function (o) {
            var check_id = o["r25:attribute_id"].$t;
            if (check_id === id) { result = o; }
        });
        return result;
    };
    
    //set a specific attribute set in a prep'ed custom attribute array
    self.setCA = function (id, caArray, obj) {
        lo.each(caArray, function (o) {
            if (o["r25:attribute_id"].$t === id) { o = obj; }
        });
    };

    //prepare the custom attribute array
    //orgs with no custom attrs have no custom_attribute set (undefined)
    //orgs with one custom attrs are represented by a {} item (single)
    //orgs with multiple custom attrs are represented by a [] array (multiple)
    self.prepCAs = function (r25Obj) {
        var caArray = r25Obj["r25:organizations"]["r25:organization"]["r25:custom_attribute"];
        if (!caArray) {
            r25Obj.organizations.organization.custom_attribute = [];
        } else {
            if (!Array.isArray(caArray)) {
                //defined but not an array (single custom attribute)
                caArray = [];
                caArray.push(r25Obj["r25:organizations"]["r25:organization"]["r25:custom_attribute"]);
                r25Obj["r25:organizations"]["r25:organization"]["r25:custom_attribute"] = caArray;
            }
        }
    };
    
    //functions for modifying custom attributes of an r25 json object
    self.updateFoundCAs = function (r25Obj, clObj) {
        self.prepCAs(r25Obj);

        var clURL = clObj.siteURL,
            now = new Date(),
            caArray = r25Obj["r25:organizations"]["r25:organization"]["r25:custom_attribute"],
            websiteCA = self.getCA(kCA0, caArray),
            firstSeenCA = self.getCA(kCA1, caArray),
            lastSeenCA = self.getCA(kCA2, caArray),
            lastScanCA = self.getCA(kCA3, caArray),
            firstSeenValue = "";
        if (!websiteCA) {
            caArray.push(self.createCA(kCA0, "Website", "R", "File Reference", clURL));
        } else {
            websiteCA.status = "mod";
            websiteCA["r25:attribute_value"].$t = clURL;
            self.setCA(kCA0, caArray, websiteCA);
        }
        if (!firstSeenCA) {
            caArray.push(self.createCA(kCA1, "CollegiateLink First Seen", "S", "String", now.toLocaleString()));
        } else {
            //check for actual date value
            firstSeenValue = firstSeenCA["r25:attribute_value"].$t;
            if (!firstSeenValue) {
                firstSeenCA.status = "mod";
                firstSeenCA["r25:attribute_value"].$t = now.toLocaleString();
                self.setCA(kCA1, caArray, firstSeenCA);
            }
        }
        if (!lastSeenCA) {
            caArray.push(self.createCA(kCA2, "CollegiateLink Last Seen", "S", "String", now.toLocaleString()));
        } else {
            lastSeenCA.status = "mod";
            lastSeenCA["r25:attribute_value"].$t = now.toLocaleString();
            self.setCA(kCA2, caArray, lastSeenCA);
        }
        if (!lastScanCA) {
            caArray.push(self.createCA(kCA3, "CollegiateLink Last Scan", "S", "String", now.toLocaleString()));
        } else {
            lastScanCA.status = "mod";
            lastScanCA["r25:attribute_value"].$t = now.toLocaleString();
            self.setCA(kCA3, caArray, lastSeenCA);
        }
        return caArray;
    };

    //functions for modifying custom attributes of an r25 json object
    self.updateNotFoundCAs = function (r25Obj) {
        self.prepCAs(r25Obj);
        var now = new Date(),
            caArray = r25Obj.organizations.organization.custom_attribute,
            lastScanCA = self.getCA(kCA3, caArray);
        if (!lastScanCA) {
            caArray.push(self.createCA(kCA2, "CollegiateLink Last Scan", "S", "String", now.toLocaleString()));
        } else {
            lastScanCA._attr.status = "mod";
            lastScanCA.attribute_value = now.toISOString();
            self.setCA(kCA3, caArray, lastScanCA);
        }
        r25Obj.organizations.organization._attr.status = "mod";
        r25Obj.organizations.organization.custom_attribute = caArray;
    };
    
    self.createCA = function (attrId, attrName, attrType, attrTypeName, attrValue) {
        var result = {};
        result.status = 'new';
        result["xl:href"] = 'orgat.xml?attribute_id=' + attrId;
        result["r25:attribute_id"] = { "$t" : attrId };
        result["r25:attribute_name"] = { "$t" : attrName };
        result["r25:attribute_type"] = { "$t" : attrType };
        result["r25:attribute_type_name"] = { "$t" : attrTypeName };
        result["r25:attribute_value"] = { "$t" : attrValue };

        return result;
    };

    self.createNewCAs = function (r25Obj, clObj) {
        var newCAs = [],
            now = new Date();
        newCAs.push(self.createCA("141", "CollegiateLink First Seen", "S", "String", now.toLocaleString()));
        newCAs.push(self.createCA("142", "CollegiateLink Last Seen", "S", "String", now.toLocaleString()));
        newCAs.push(self.createCA("143", "CollegiateLink Last Scan", "S", "String", now.toLocaleString()));
        return newCAs;
    };
    ////end of custom attribute modification methods
    
    
    //preparation of our two data lists (CollegiateLink RSOs and Resource25 RSOs
    self.updateR25OrgInfo = function (o) {
        var orgId = o.json["r25:organizations"]["r25:organization"]["r25:organization_id"].$t,
            orgInfo = o.json;
        r25Orgs[orgId] = orgInfo;
    };
    //r25.on('getOrg', updateR25OrgInfo);

    self.updateR25OrgSets = function (o) {
        var orgId = o["r25:organization_id"].$t,
            orgName = o["r25:organization_title"].$t;
        
        if (!orgName) {
            orgName = o["r25:organization_name"].$t;
		    //app.log.info("Updating r25OrgList with name: " + orgName);
        } else {
            //app.log.info("Updating r25OrgList with title: " + orgName);
        }
        orgName = orgName.toUpperCase();
        r25OrgList[orgName] = orgId;
        self.prepR25Wait = self.prepR25Wait - 1;
        if (self.prepR25Wait <= 0) {
            self.emit("prepR25OrgsDONE");
        }
    };

    self.prepR25Orgs = function (o) {
        var orgs = o.json;
        self.prepR25Wait = orgs.length;
        app.log.info(orgs.length, " total Resource25 Organization(s) found.");
        lo.each(orgs, function (o) { self.updateR25OrgSets(o); }, function () {
            //app.log.info("COMPLETE!");
        });
        //self.emit("prepR25OrgsDONE");
    };
    
    r25.on('getOrgs', self.prepR25Orgs);
    
    self.updateCLinkOrg = function (o) {
        cLOrgs[o.name.toUpperCase()] = o;
    };
    clink.on('orgInfo', self.updateCLinkOrg);

    self.prepCLinkOrgs = function () {
        clink.on('parseDone', function () {
            self.emit('prepCLinkOrgsDONE');
        });
        clink.request();
    };

    
    /*
     * updateOrg - Updates a Resource25 Organization record
     *
     * Sets required categories - 
     * Sets required custom attributes -
     */
    self.updateOrg = function (orgId, cLinkOrg, callback) {
        //app.log.info("FOUND org: " + orgId + ":" + cLinkOrg.name);
		r25.getOrg(orgId, function (r25Obj) {
            var r25key = "";
            r25Obj = r25Obj.json;
            if (!r25Obj) {
                callback();
            } else {
                //remove obj from our r25_orgs and r25_org_list
                r25Obj["r25:organizations"]["r25:organization"].status = "mod";
                r25Obj["r25:organizations"]["r25:organization"]["r25:custom_attribute"] = self.updateFoundCAs(r25Obj, cLinkOrg);
        
                r25.putOrg(orgId, r25Obj, function () {
                    //on success, remove this item from our lists
                    //app.log.debug("PUT org complete: " + orgId);
                    r25key = r25Obj["r25:organizations"]["r25:organization"]["r25:organization_title"].$t;
                    if (!r25key) {
                        r25key = r25Obj["r25:organizations"]["r25:organization"]["r25:organization_name"].$t;
                    }
                    r25key = r25key.toUpperCase().trim();
                    //app.log.info("attempting to remove object for key: " + r25key);
                    var foundObj = r25OrgList[r25key];
                    delete r25OrgList[r25key];
                    delete cLOrgs[cLinkOrg.name.toUpperCase().trim()];
                    r25UpdatedOrgs[orgId] = r25Obj;
                    callback();
                }, function (errObj) {
                    //handle error
                    self.syncErrors[errObj.id] = errObj;
                    delete r25OrgList[cLinkOrg.name.toUpperCase().trim()];
                    delete cLOrgs[cLinkOrg.name.toUpperCase().trim()];
                    callback();
                });
            }
        });
    };
    
    /* NOT IMPLEMENTED YET
    updateNotFoundOrg = function (orgId, callback) {
        var r25Obj = r25Orgs[orgId];
        self.updateNotFoundCAs(r25Obj);
        r25.putOrg(orgId, r25Obj, function() {
            callback();            
        }, function(errObj) {
            syncErrors[errObj.id] = errObj;
            callback();
        });
    }
    */
    
    self.evaluateClinkOrg = function (o) {
        var cLinkOrgName = o.name.toUpperCase().trim(),
            orgId = -1,
            r25Org = {};

        //app.log.info("Attempting to find: " + cLinkOrgName);
        orgId = r25OrgList[cLinkOrgName];
        //app.log.info("Searching for: " + cLinkOrgName + " : " + orgId);
        if (!orgId) {
            cLinkOrgName = o.full_name.toUpperCase().trim();
            orgId = r25OrgList[cLinkOrgName];
            //app.log.info("Searching for: " + cLinkOrgName + " : " + orgId);
        }
        if (!orgId) {
            //one last search for substring 0-79 as R25 supports a max of 80 chars in title
            cLinkOrgName = cLinkOrgName.substring(0, 80);
            orgId = r25OrgList[cLinkOrgName];
            //app.log.info("Searching for: " + cLinkOrgName + " : " + orgId);
        }
        if (orgId > 0) {
            self.updateOrg(orgId, o, function () {
                self.prepCLWait = self.prepCLWait - 1;
                if (self.prepCLWait <= 0) {
                    //app.log.info("EMIT processCLinkDataDONE");
                    self.emit("processCLinkDataDONE");
                }
            });
        } else {
            self.prepCLWait = self.prepCLWait - 1;
            if (self.prepCLWait <= 0) {
                //app.log.info("EMIT processCLinkDataDONE");
                self.emit("processCLinkDataDONE");
            }
            //never gets called (should)
            //app.log.debug("need to update LAST SCAN!" + orgId);
            /*
            updateNotFoundOrg(orgId, function() {
                //nothing to be done on callback                
            });
            */
        }
    };

    self.processCLinkData = function () {
        self.prepCLWait = lo.keys(cLOrgs).length;
        app.log.info(self.prepCLWait, " total CollegiateLink Organization(s) found.");
        lo.each(cLOrgs, function (o) {
            self.evaluateClinkOrg(o);
        });
    };
    
    self.addToSuperSet = function (id, name, siteURL, type) {
        //app.log.debug("adding[" + type + "]: " + name);
        var newItem = {};
        newItem.id = id;
        newItem.name = name;
        newItem.siteURL = siteURL;
        newItem.type = type;
        superSet[name] = newItem;
    };
    
    self.generateReport = function () {
        var model = {},
            reportResults = {},
            r25ws = app.config.get("r25ws"),
            r25OrgNames = lo.keys(r25OrgList);
        
        model.org_BROWSE = "http://" + r25ws.r25ws_url + r25ws.base_uri + r25ws.org_BROWSE;
        model.org_BROWSE = r25ws.r25_25live_org_url;
        
        app.log.debug("CLink Org count for report: " + lo.keys(cLOrgs).length);
        lo.each(cLOrgs, function (o) {
            self.addToSuperSet(o.cl_id, o.name, o.siteURL, "clink");
        });
        
        app.log.debug("R25 Org count for report: " + lo.keys(r25OrgList).length);
        lo.each(r25OrgNames, function (name) {
            self.addToSuperSet(r25OrgList[name], name, null, "r25");
        });
        
        model.orgs = superSet;
        model.title = "Organization Results";
        model.action = "Please review the following table for unresolved organizations:";
        reportResults.orgTable = report.generateReport(model, "tmpl/table.html");

        model.orgs = r25UpdatedOrgs;
        model.title = "RSOs found in CollegiateLink AND Resource25";
        model.action = "No action/follow-up required";
        reportResults.clinkupdated = report.generateReport(model, "tmpl/r25org.html");
        
        model.orgs = self.syncErrors;
        model.title = "Sync errors";
        model.action = "Please forward this information to your R25 administrator:";
        reportResults.syncerrors = report.generateReport(model, "tmpl/error.html");

        self.reportContent = report.generateReport(reportResults, "tmpl/email.html");
        //app.log.info("generate final report");
        fs.writeFile("report.html", self.reportContent, function (err) {
            if (err) {
                app.log.error(err);
            }
            self.emit("reportGenerationDONE");
        });
    };

    self.emailReport = function () {
        var emailOptions = app.config.get("email");
        emailOptions.html = self.reportContent;
        nmailer.SMTP = { host: emailOptions.host };
        nmailer.send_mail({
            sender: emailOptions.sender,
            to: emailOptions.to,
            subject: emailOptions.subject,
            html: emailOptions.html,
            body: emailOptions.body
        }, function (mailErr, success) {
            if (success) {
                app.log.info("Report email sent.");
            }
            if (mailErr) {
                app.log.error('Message ' + success ? 'sent' : 'failed');
                inspect(mailErr);
            }
            self.emit("emailReportDONE");
        });
    };

    self.addGetError = function (errObj) {
        self.syncErrors[errObj.id] = errObj;
        self.prepR25Wait = self.prepR25Wait - 1;
        if (self.prepR25Wait <= 0) {
            self.emit("prepR25OrgsDONE");
        }
    };
    self.addGetOrgsError = function (errObj) {
        self.syncErrors[errObj.id] = errObj;
        self.emit("prepR25OrgsDONE");
    };
    
    //general flow of the app:
    r25.getOrgs();
    r25.on("getError", self.addGetError);
    r25.on("getOrgsError", self.addGetOrgsError);

    this.on("prepR25OrgsDONE", self.prepCLinkOrgs);
    this.on("prepCLinkOrgsDONE", self.processCLinkData);
    this.on("processCLinkDataDONE", self.generateReport);
    this.on("reportGenerationDONE", self.emailReport);
    this.on("emailReportDONE", function () {
        //app.log.info("Reporting done!");
        process.exit();
    });
};

util.inherits(Sync, events.EventEmitter);
module.exports = new Sync();
