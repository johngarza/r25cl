var flatiron = require('flatiron'),
	app = flatiron.app,
	events = require('events'),
    fs = require('fs'),
    _ = require('underscore'),
    clink = require('../clink'),
    util = require('util');

var Export = function () {
    'use strict';
    events.EventEmitter.call(this);
    //general flow of things

    var self = this,
        export_target = "export.txt",
        export_str = 'name\tstatus\taccount\tein\tbulletin board 2012-2013\tworkspace 2012-2013\tget oriented session\tSMRMT 2012-2013\tSMRMT Plan\n';
    /* public methods
    */
    this.cl = function (org_id) {
        app.log.info("Exporting from CollegiateLink");
        clink.on('orgInfo', function (o) {
            export_str = export_str + '"' + o.name + '"\t ' + o.status + '\t' + o.account_id + '\t' + o.ein + '\t' + o.bba + '\t' + o.wa2012 + '\t' + o.gosa + '\t' + o.smrmt + '\t' + o.smrmt_plan + '\n';
        });
        clink.on('parseDone', function () {
            self.emit('writeReady', export_str);
        });
        clink.request();
    };

    self.updateFile = function (orgInfoStr) {
        fs.writeFile(export_target, orgInfoStr, function (writeErr) {
            if (writeErr) {
                app.log.error("ERROR while writing file: ", writeErr);
            }
        });
    };
    
    this.on('writeReady', self.updateFile);
};

util.inherits(Export, events.EventEmitter);
module.exports = new Export();
