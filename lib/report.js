var flatiron = require('flatiron'),
	app = flatiron.app,
	events = require('events'),
    fs = require('fs'),
    lo = require('underscore'),
    util = require('util');

var Report = function () {
    'use strict';
    events.EventEmitter.call(this);
    //general flow of things

    var self = this;
    
    self.generateReport = function (model, template) {
        var result = "",
            data = fs.readFileSync(template, "UTF-8"),
            jobTmpl = lo.template(data);
        result = jobTmpl(model);
        self.emit("generatedReport", result);
        return result;
    };
};

util.inherits(Report, events.EventEmitter);
module.exports = new Report();