#!/usr/bin/node

var flatiron = require('flatiron'),
    path = require('path'),
    app = flatiron.app;

app.config.file({ file: path.join(__dirname, 'config', 'config.json') });

app.use(flatiron.plugins.cli, {
  source: path.join(__dirname, 'lib', 'commands'),
  usage: 'sync'
});


app.use(require('flatiron-cli-config'));

app.DEBUG = app.config.get('debug');

app.start();
