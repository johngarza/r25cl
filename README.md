r25cl
=====

Resource25 + CollegiateLink Sync Application
--------------------------------------------

Small nodejs utility application to sync up Resource25 organizations with a CollegiateLink list of organizations.


Requirements
------------

1) Resource25 WebServices 2.3 or higher (tested on 2.3)

2) CollegiateLink API access key (see config.json)


Required Reading:
-----------------

Resrouce25 WebService Documentation
- http://knowledge25.collegenet.com/display/WSW/Home

CollegiateLink WebService Documentation
- http://support.collegiatelink.net/entries/332558-web-services-developer-documentation


Running the Application:
------------------------

1) Install dependencies with an `npm update

2) Set your configuration in config/config.json using the example config.json

3) Run the sync with `node app sync`

